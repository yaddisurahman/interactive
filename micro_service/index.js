var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var mailer = require('./SendEmail');

app.use(urlencodedParser)

var fs = require('fs');
// app.use(mailer);

  app.get('/', function (req, res) {
    res.send('Hello World!')
  });

  // 1st micro service
  app.post('/send_the_email', function (req, res) {
    res.send('Got a POST request');
    mailer.send_email(req, res);
  });

  // 2nd micro service
  // app.post('/send_email', urlencodedParser, function(req, res) {
  //     console.log(req.body);
  //     // console.log(res);
  //     res.send('')
  // });

  app.post('/send_email', invoke_email)

  function invoke_email(req, res, next) {
    console.log(req.body);
    var time = new Date().getTime();
    var content = JSON.stringify(req.body);
    fs.writeFile(__dirname+"/output/"+time, content, function(err) {
      if(err) {
          return console.log(err);
      };
    });

     req.url = '/send_the_email';
     //if we want to change the method: req.method = 'POST'
     return app._router.handle(req, res, next)
  }


app.listen(port);
console.log('todo list RESTful API server started on: ' + port);
