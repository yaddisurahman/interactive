module.exports = {

  send_email: function(req, res) {

    'use strict';
    const nodemailer = require('nodemailer');

    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    nodemailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        var transporter = nodemailer.createTransport({
          host: "smtp.mailtrap.io",
          port: 2525,
          auth: {
            user: "da9c5b69f8477a",
            pass: "9ff08a8ce136b6"
          }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Yaddi Surahman" <foo@example.com>', // sender address
            to: req.body.email_to, // list of receivers
            subject: req.body.subject, // Subject line
            text: req.body.content+ '<br>dikirim pada:' + req.body.timestamp, // plain text body
            html: req.body.content+ '<br>dikirim pada:' + req.body.timestamp // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    });

  }
}
