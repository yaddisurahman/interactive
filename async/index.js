var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var rp = require('request-promise');

    var options = {
      method: 'POST',
      uri: 'https://test.interaktiv.sg/robot-test/',
      headers: {
         "Authorization":"test-robot-interaktiv",
         "email": "yaddisurahman@gmail.com",
         "Content-Type":"application/json"
      },
      json: true // Automatically stringifies the body to JSON
    };

    for (var i = 1; i <= 5; i++) {
      options.body = {request: [i]}
      console.log(options);
      rp(options)
      .then(function (parsedBody) {
        // POST succeeded...
        console.log(parsedBody)
      })
      .catch(function (err) {
        // POST failed...
        console.log(err)
      });
    }

console.log('todo list RESTful API server started on: ' + port);
